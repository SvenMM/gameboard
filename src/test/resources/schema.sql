drop table if exists Beers;
drop table if exists Brewers;
drop table if exists Categories;
drop table if exists BeerOrderItems;
drop table if exists BeerOrders;

Create table if not exists Brewers (
    Id integer IDENTITY not null,
    Name varchar(50),
    Address varchar(50),
    ZipCode varchar(6),
    City varchar(50),
    Turnover integer,
    primary key (Id)
    );
CREATE TABLE IF NOT EXISTS  Categories (
    Id integer IDENTITY not null,
    Category varchar(100),
    primary key(Id)
    );
Create table if not exists Beers (
    Id integer IDENTITY not null,
    Name varchar(100),
    BrewerId integer,
    CategoryId integer,
    Price float,
    Stock integer,
    Alcohol float,
    Version integer,
    Image LONGVARBINARY,
    primary key (Id),
    foreign key (BrewerId) references Brewers(Id),
    foreign key (CategoryId) references Categories(Id)
    );
CREATE TABLE IF NOT EXISTS  BeerOrders (
    Id integer IDENTITY not null,
    Name varchar(100),
    primary key(Id)
    );
CREATE TABLE IF NOT EXISTS  BeerOrderItems (
    Id integer IDENTITY not null,
    Number integer,
    BeerId integer,
    BeerOrderId integer,
    primary key (Id),
    foreign key (BeerOrderId) references BeerOrders(Id)
    );
