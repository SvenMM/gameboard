truncate table Brewers;
truncate table Categories;
truncate table Beers;
truncate table BeerOrderItems;
truncate table BeerOrders;

insert into Brewers values (1, 'TestBrewer', 'TestStreet', 1000, 'TestCity', 10000);
insert into Categories values (1, 'TestCategory');
insert into Beers Values (1, 'TestBeer', 1, 1, 2.75, 100, 7, 0, null);
insert into BeerOrders values (1, 'TestOrder');
insert into BeerOrderItems values (1, 4, 1, 1);