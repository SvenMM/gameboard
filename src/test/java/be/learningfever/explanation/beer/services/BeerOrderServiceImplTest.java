package be.learningfever.explanation.beer.services;

import be.learningfever.explanation.beer.entities.*;
import be.learningfever.explanation.beer.exceptions.BeerNotFoundException;
import be.learningfever.explanation.beer.exceptions.InvalidOrderException;
import be.learningfever.explanation.beer.repositories.BeerOrderRepository;
import be.learningfever.explanation.beer.repositories.BeerRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Sven Wittoek
 * created on Tuesday, 04/05/2021
 */
@ExtendWith(MockitoExtension.class)
class BeerOrderServiceImplTest {
    @Mock
    private BeerOrderRepository beerOrderRepository;
    @Mock
    private BeerRepository beerRepository;
    @InjectMocks
    private BeerOrderServiceImpl beerOrderService;
    @Mock
    private Beer beer;

    @Test
    void createBeerOrderItemChangesStock(){
        Beer beer = new Beer();
        beer.setStock(25);

        BeerOrderServiceImpl beerOrderServiceSpy = Mockito.spy(beerOrderService);
        Mockito.doNothing().when(beerOrderServiceSpy).checkIfOrderIsPossible(Mockito.anyInt(), Mockito.any(Beer.class));
        Mockito.when(beerRepository.findById(1L)).thenReturn(Optional.of(beer));

        BeerOrderItem actualBeerOrderItem = beerOrderServiceSpy.createBeerOrderItem(1, 5);

        Assertions.assertEquals(20, actualBeerOrderItem.getBeer().getStock());
    }

    @Test
    void createBeerOrder(){
        BeerOrderItem beerOrderItem = new BeerOrderItem(beer, 4);
        BeerOrder expectedBeerOrder = new BeerOrder("test", List.of(beerOrderItem));

        BeerOrder actualBeerOrder= beerOrderService.createBeerOrder("test", List.of(beerOrderItem));

        Assertions.assertEquals(expectedBeerOrder, actualBeerOrder);
    }

    @Test
    void checkIfOrderIsPossible(){
        Mockito.when(beer.getStock()).thenReturn(10);
        Assertions.assertDoesNotThrow(() -> beerOrderService.checkIfOrderIsPossible(5, beer));
    }

    @Test
    void checkIfOrderIsPossibleThrowsBNFEWhenNull(){
        Assertions.assertThrows(BeerNotFoundException.class, () -> beerOrderService.checkIfOrderIsPossible(5, null));
    }

    @Test
    void checkIfOrderIsPossibleThrowsIOEWhenBeerStockLowerThenNumberOfBeers(){
        Mockito.when(beer.getStock()).thenReturn(3);
        Assertions.assertThrows(InvalidOrderException.class, () -> beerOrderService.checkIfOrderIsPossible(5, beer));
    }

    @Test
    void checkIfOrderIsPossibleThrowsIOEWhenNumberOfBeersIsZero(){
        Assertions.assertThrows(InvalidOrderException.class, () -> beerOrderService.checkIfOrderIsPossible(0, beer));
    }

    @Test
    void checkIfOrderIsPossibleThrowsIOEWhenNumberOfBeersIsLowerThanZero(){
        Assertions.assertThrows(InvalidOrderException.class, () -> beerOrderService.checkIfOrderIsPossible(-1, beer));
    }


    @Test
    void orderBeers() {
        BeerOrderServiceImpl beerOrderServiceSpy = Mockito.spy(this.beerOrderService);
        Mockito.when(beerRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(beer));
        Mockito.doNothing().when(beerOrderServiceSpy).checkIfOrderIsPossible(Mockito.anyInt(), Mockito.any(Beer.class));
        BeerOrder expectedBeerOrder= new BeerOrder("test", List.of(new BeerOrderItem(beer, 4)));

        beerOrderServiceSpy.orderBeers("test", 1, 4);

        Mockito.verify(beerOrderRepository, Mockito.times(1)).save(expectedBeerOrder);
        Mockito.verify(beerOrderServiceSpy, Mockito.times(1)).checkIfOrderIsPossible(4, beer);
        Mockito.verify(beerOrderServiceSpy, Mockito.times(1)).createBeerOrderItem(1, 4);
    }

    @Test
    void oderBeers() {
        BeerOrderServiceImpl beerOrderServiceSpy = Mockito.spy(this.beerOrderService);
        Mockito.when(beerRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(beer));
        Mockito.doNothing().when(beerOrderServiceSpy).checkIfOrderIsPossible(Mockito.anyInt(), Mockito.any(Beer.class));
        Map<Long, Integer> beerOrders = new HashMap<>();
        beerOrders.put(1L, 4);
        beerOrders.put(2L, 3);
        BeerOrder expectedBeerOrder= new BeerOrder("test", List.of(new BeerOrderItem(beer, 4), new BeerOrderItem(beer, 3)));

        beerOrderServiceSpy.orderBeers("test", beerOrders);

        Mockito.verify(beerOrderRepository, Mockito.times(1)).save(expectedBeerOrder);
        Mockito.verify(beerOrderServiceSpy, Mockito.times(2)).checkIfOrderIsPossible(Mockito.anyInt(), Mockito.any(Beer.class));
        Mockito.verify(beerOrderServiceSpy, Mockito.times(2)).createBeerOrderItem(Mockito.anyLong(), Mockito.anyInt());
    }
}