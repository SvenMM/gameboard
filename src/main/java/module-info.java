open module be.learningfever.explanation {
    requires spring.context;
    requires spring.core;
    requires spring.beans;

    requires spring.boot;
    requires spring.boot.autoconfigure;
//    Nodig voor gebruik van JPA
    requires spring.boot.starter.data.jpa;
    requires org.hibernate.orm.core;

    requires java.sql;
    requires java.annotation;
    requires java.xml.bind;
    requires java.validation;
    //    Maakt opmaak persistente objecten mogelijk (entities)
    requires java.persistence;

    requires net.bytebuddy;
    requires com.sun.xml.bind;
    requires com.fasterxml.jackson.annotation;
    requires org.apache.tomcat.embed.core;
}