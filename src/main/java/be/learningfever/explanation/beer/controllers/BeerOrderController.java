package be.learningfever.explanation.beer.controllers;

import be.learningfever.explanation.beer.entities.BeerOrder;
import be.learningfever.explanation.beer.entities.BeerOrderHelper;
import be.learningfever.explanation.beer.services.BeerOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Sven Wittoek
 * created on Tuesday, 04/05/2021
 */
@RestController
@RequestMapping("/Beers/Orders")
public class BeerOrderController {
    private BeerOrderService beerOrderService;

    public BeerOrderService getBeerOrderService() {
        return beerOrderService;
    }

    @Autowired
    public void setBeerOrderService(BeerOrderService beerOrderService) {
        this.beerOrderService = beerOrderService;
    }

    @GetMapping(path = "{id:^\\d+$}")
    public ResponseEntity<BeerOrder> findBeerOrder(@PathVariable long id) {
        BeerOrder order = beerOrderService.findBeerOrder(id);
        return new ResponseEntity<>(order, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity saveBeerOrder(@Valid @RequestBody BeerOrderHelper beerOrderHelper, HttpServletRequest request) {
        Map<Long, Integer> orderItems = new HashMap<>();
        for (int[] item: beerOrderHelper.getOrderItems()) {
            orderItems.put((long) item[BeerOrderHelper.BEER_ID_INDEX], item[BeerOrderHelper.AMOUNT_OF_BEERS_INDEX]);
        }
        long id = beerOrderService.orderBeers(beerOrderHelper.getName(), orderItems);
        URI uri = URI.create(request.getRequestURL() + "/" + id);
        return ResponseEntity.created(uri).build();
    }


}
