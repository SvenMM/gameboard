package be.learningfever.explanation.beer.controllers;

import be.learningfever.explanation.beer.entities.Beer;
import be.learningfever.explanation.beer.entities.BeerList;
import be.learningfever.explanation.beer.exceptions.BeerNotFoundException;
import be.learningfever.explanation.beer.services.BeerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URI;

/**
 * @author Sven Wittoek
 * created on Tuesday, 04/05/2021
 */
@RestController
@RequestMapping("/beers")
public class BeerController {
    private BeerService beerService;

    public BeerService getBeerService() {
        return beerService;
    }

    @Autowired
    public void setBeerService(BeerService beerService) {
        this.beerService = beerService;
    }

    @GetMapping
    public ResponseEntity<BeerList> findAllBeers() {
        BeerList beers = beerService.findAllBeers();
        return new ResponseEntity<>(beers, HttpStatus.OK);
    }

    @GetMapping(params = "alcohol")
    public ResponseEntity<BeerList> findAllBeersByAlcohol(@RequestParam float alcohol) {
        BeerList beers = beerService.findAllBeersByAlcohol(alcohol);
        return new ResponseEntity<>(beers, HttpStatus.OK);
    }

    @GetMapping(params = "name")
    public ResponseEntity<BeerList> findAllBeersByName(@RequestParam String name) {
        BeerList beers = beerService.findAllBeersWithNameContaining(name);
        return new ResponseEntity<>(beers, HttpStatus.OK);
    }

    @GetMapping(path = "{id:^\\d+$}")
    public ResponseEntity<Beer> findBeer(@PathVariable long id) {
        Beer beer = beerService.findBeer(id);
        return new ResponseEntity<>(beer, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity createNewBeer(@Valid @RequestBody Beer beer, HttpServletRequest request) {
        beerService.saveBeer(beer);
        URI uri = URI.create(request.getRequestURL() + "/" + beer.getId());
        return ResponseEntity.created(uri).build();
    }

    @PutMapping(path = "{id:^\\d+$}")
    public ResponseEntity updateBeer(@PathVariable long id, @Valid @RequestBody Beer beer, HttpServletRequest request) {
        checkIfBeerIdIsValid(id, beer);
        beerService.saveBeer(beer);
        return ResponseEntity.ok().build();
    }

    private void checkIfBeerIdIsValid(@PathVariable long id, @RequestBody @Valid Beer beer) throws BeerNotFoundException {
        if (id != beer.getId()) {
            throw new BeerNotFoundException();
        }
    }

    @PatchMapping(path = "{id:^\\d+$}")
    public ResponseEntity updateBeerPartially(@PathVariable long id, @RequestBody Beer beer, HttpServletRequest request) {
        checkIfBeerIdIsValid(id, beer);
        Beer original = beerService.findBeer(id);
        updateBeer(beer, original);
        beerService.saveBeer(original);
        return ResponseEntity.ok().build();
    }

    private void updateBeer(Beer beer, Beer original) {
        if (beer.getName() != null) {
            original.setName(beer.getName());
        }
        if (beer.getPrice() > 0) {
            original.setPrice(beer.getPrice());
        }
        if (beer.getImage() != null) {
            original.setImage(beer.getImage());
        }
        if (beer.getAlcohol() > 0) {
            original.setAlcohol(beer.getAlcohol());
        }
        if (beer.getStock() > 0) {
            original.setStock(beer.getStock());
        }
        if (beer.getBrewer() != null) {
            original.setBrewer(beer.getBrewer());
        }
        if (beer.getCategory() != null) {
            original.setCategory(beer.getCategory());
        }
    }

}
