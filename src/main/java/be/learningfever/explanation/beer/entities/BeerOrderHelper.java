package be.learningfever.explanation.beer.entities;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Sven Wittoek
 * created on Tuesday, 04/05/2021
 */
@XmlRootElement
public class BeerOrderHelper {
    public static final int BEER_ID_INDEX=0;
    public static final int AMOUNT_OF_BEERS_INDEX=1;
    @NotBlank
    private String name;
    @NotNull
    private int[][] orderItems;

    public BeerOrderHelper() {
    }

    public BeerOrderHelper(String name, int[][] orderItems) {
        this.name = name;
        this.orderItems = orderItems;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlElementWrapper(name = "beersOrderItems")
    @XmlElement(name = "beerOrderItem")
    public int[][] getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(@NotNull int[][] orderItems) {
        this.orderItems = orderItems;
    }

}
