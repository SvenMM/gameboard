package be.learningfever.explanation.beer.entities;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Entity
@Table(name = "BeerOrders")
@XmlRootElement
public class BeerOrder implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private long id;
    @Column(name = "Name")
    private String name;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "BeerOrderId")
    private List<BeerOrderItem> items = new ArrayList<>();

    public BeerOrder() {
    }

    public BeerOrder(String name, List<BeerOrderItem> items) {
        this.name = name;
        this.items = items;
    }

    public BeerOrder(long id, String name, List<BeerOrderItem> items) {
        this.id = id;
        this.name = name;
        this.items = items;
    }

    public long getId() {
        return id;
    }
    private void setId(long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public List<BeerOrderItem> getItems() {
        return items;
    }
    public void setItems(List<BeerOrderItem> items) {
        this.items = items;
    }
    @Override
    public String toString() {
        return String.format("%d: %s%nItems: %s%n",
                getId(),
                getName(),
                getItems().stream().map(el -> el.getBeer().getName()).collect(Collectors.toList())
        );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BeerOrder beerOrder = (BeerOrder) o;
        return Objects.equals(name, beerOrder.name) && Objects.equals(items, beerOrder.items);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, items);
    }
}
