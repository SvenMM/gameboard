package be.learningfever.explanation.beer.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "Brewers")
@XmlRootElement
public class Brewer implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private long id;
    @Column(name = "Name")
    private String name;
    @Column(name = "Address")
    private String address;
    @Column(name = "ZipCode")
    private String zipCode;
    @Column(name = "City")
    private String city;
    @Column(name = "Turnover")
    private int turnover;
    @OneToMany(mappedBy = "brewer", cascade = {CascadeType.PERSIST, CascadeType.PERSIST, CascadeType.DETACH, CascadeType.REFRESH})
    private Set<Beer> beers = new HashSet<>();
    public Brewer() {
    }
    public Brewer(String name, String address, String zipCode, String city, int turnover) {
        this(name, address, zipCode, city, turnover, new HashSet<>());
    }
    public Brewer(String name, String address, String zipCode, String city, int turnover, Set<Beer> beers) {
        setName(name);
        setAddress(address);
        setZipCode(zipCode);
        setCity(city);
        setTurnover(turnover);
        setBeers(beers);
    }
    public long getId() {
        return id;
    }
    private void setId(long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public String getZipCode() {
        return zipCode;
    }
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public int getTurnover() {
        return turnover;
    }
    public void setTurnover(int turnover) {
        this.turnover = turnover;
    }
    @XmlTransient
    @JsonIgnore
    public Set<Beer> getBeers() {
        return beers;
    }
    public void setBeers(Set<Beer> beers) {
        this.beers = beers;
    }
    @Override
    public String toString() {
        return String.format("%d: %s%nAddress: %s, %s %s%nTurnover: %d%nBrews: %s%n",
                getId(),
                getName(),
                getAddress(),
                getZipCode(),
                getCity(),
                getTurnover(),
                getBeers().stream().map(Beer::getName).collect(Collectors.toList())
        );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Brewer brewer = (Brewer) o;
        return turnover == brewer.turnover && Objects.equals(name, brewer.name) && Objects.equals(address, brewer.address) && Objects.equals(zipCode, brewer.zipCode) && Objects.equals(city, brewer.city);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, address, zipCode, city, turnover);
    }
}
