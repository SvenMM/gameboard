package be.learningfever.explanation.beer.entities;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;

@Entity
@Table(name = "Beers")
@NamedQuery(name = "updateBeerPrice", query = "update Beer b set b.price = b.price * (1 + (100/:percentage))")
@XmlRootElement
public class Beer implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private long id;
    @Version
    @Column(name = "Version")
    private int version;
    @Column(name = "Name")
    private String name;
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH})
    @JoinColumn(name = "BrewerId")
    private Brewer brewer;
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH})
    @JoinColumn(name = "CategoryId")
    private Category category;
    @Column(name = "Price")
    private double price;
    @Column(name = "Stock")
    private int stock;
    @Column(name = "Alcohol")
    private float alcohol;
    @Column(name = "Image")
    @Lob
    private byte[] image;
    public Beer() {
    }
    public Beer(String name, Brewer brewer, Category category, double price, int stock, float alcohol) {
        this(name, brewer, category, price, stock, alcohol, null);
    }
    public Beer(String name, Brewer brewer, Category category, double price, int stock, float alcohol, byte[] image) {
        setName(name);
        setBrewer(brewer);
        setCategory(category);
        setPrice(price);
        setStock(stock);
        setAlcohol(alcohol);
        setImage(image);
    }
    public long getId() {
        return id;
    }
    private void setId(long id) {
        this.id = id;
    }
    public int getVersion() {
        return version;
    }
    private void setVersion(int version) {
        this.version = version;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Brewer getBrewer() {
        return brewer;
    }
    public void setBrewer(Brewer brewer) {
        this.brewer = brewer;
    }
    public Category getCategory() {
        return category;
    }
    public void setCategory(Category category) {
        this.category = category;
    }
    public double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        this.price = price;
    }
    public int getStock() {
        return stock;
    }
    public void setStock(int stock) {
        this.stock = stock;
    }
    public float getAlcohol() {
        return alcohol;
    }
    public void setAlcohol(float alcohol) {
        this.alcohol = alcohol;
    }
    public byte[] getImage() {
        return image;
    }
    public void setImage(byte[] image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Beer{" +
                "id=" + id +
                ", version=" + version +
                ", name='" + name + '\'' +
                ", brewer=" + brewer +
                ", category=" + category +
                ", price=" + price +
                ", stock=" + stock +
                ", alcohol=" + alcohol +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Beer beer = (Beer) o;
        return Double.compare(beer.price, price) == 0 && stock == beer.stock && Float.compare(beer.alcohol, alcohol) == 0 && Objects.equals(name, beer.name) && Objects.equals(brewer, beer.brewer) && Objects.equals(category, beer.category) && Arrays.equals(image, beer.image);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name, brewer, category, price, stock, alcohol);
        result = 31 * result + Arrays.hashCode(image);
        return result;
    }
}
