package be.learningfever.explanation.beer.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "Categories")
@XmlRootElement
public class Category implements Serializable {
    @Id
    @Column(name = "Id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "Category")
    private String name;
    @OneToMany(mappedBy = "category")
    private Set<Beer> beers = new HashSet<Beer>();

    public Category() {
    }
    public Category(String name) {
        this(name, new HashSet<Beer>());
    }
    public Category(String name, Set<Beer> beers) {
        setName(name);
        setBeers(beers);
    }
    public long getId() {
        return id;
    }
    private void setId(long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    @XmlTransient
    @JsonIgnore
    public Set<Beer> getBeers() {
        return beers;
    }
    public void setBeers(Set<Beer> beers) {
        this.beers = beers;
    }
    @Override
    public String toString() {
        return String.format("%d: %s%nBeers: %s%n",
                getId(),
                getName(),
                getBeers().stream().map(Beer::getName).collect(Collectors.toList())
        );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Category category = (Category) o;
        return Objects.equals(name, category.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
