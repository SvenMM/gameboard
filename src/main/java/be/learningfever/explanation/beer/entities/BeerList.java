package be.learningfever.explanation.beer.entities;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * @author Sven Wittoek
 * created on Tuesday, 04/05/2021
 */
@XmlRootElement
public class BeerList {
    private List<Beer> beers;

    public BeerList() {
    }

    public BeerList(List<Beer> beers) {
        this.beers = beers;
    }

    @XmlElementWrapper(name = "beers")
    @XmlElement(name = "beer")
    public List<Beer> getBeers() {
        return beers;
    }

    public void setBeers(List<Beer> beers) {
        this.beers = beers;
    }

}
