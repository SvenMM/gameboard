package be.learningfever.explanation.beer.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Sven Wittoek
 * created on Tuesday, 04/05/2021
 */
@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Beer could not be found")
public class BeerNotFoundException extends RuntimeException{
    public BeerNotFoundException() {
    }

    public BeerNotFoundException(String message) {
        super(message);
    }

    public BeerNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public BeerNotFoundException(Throwable cause) {
        super(cause);
    }
}
