package be.learningfever.explanation.beer.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Sven Wittoek
 * created on Tuesday, 04/05/2021
 */
@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class InvalidOrderException extends RuntimeException{
    public InvalidOrderException() {
    }

    public InvalidOrderException(String message) {
        super(message);
    }

    public InvalidOrderException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidOrderException(Throwable cause) {
        super(cause);
    }
}
