package be.learningfever.explanation.beer.repositories;

import be.learningfever.explanation.beer.entities.BeerOrder;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Sven Wittoek
 * created on Tuesday, 04/05/2021
 */
public interface BeerOrderRepository extends JpaRepository<BeerOrder, Long> {
    public BeerOrder getBeerOrderById(long id);
}
