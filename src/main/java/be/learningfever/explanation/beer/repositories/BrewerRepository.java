package be.learningfever.explanation.beer.repositories;

import be.learningfever.explanation.beer.entities.Brewer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Sven Wittoek
 * created on Tuesday, 04/05/2021
 */
public interface BrewerRepository extends JpaRepository<Brewer, Long> {
    List<Brewer> findAllByTurnoverGreaterThan(int turnover);
}
