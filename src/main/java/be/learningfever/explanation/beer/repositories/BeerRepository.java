package be.learningfever.explanation.beer.repositories;

import be.learningfever.explanation.beer.entities.Beer;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

public interface BeerRepository extends JpaRepository<Beer, Long> {
    List<Beer> findAllBeersByAlcohol(float alcohol);
    List<Beer> findAllByNameContaining(String name);
    List<Beer> findAllByStock(int stock);
    List<Beer> findAllByBrewer_AddressContaining(String address);
    @Query(name = "updateBeerPrice")
    void updateBeerPriceByPercentage(int percentage);

    @Transactional
    public default List<Beer> findAllBeersByExample(Beer beer) {
        ExampleMatcher exampleMatcher = ExampleMatcher.matchingAny()
                .withIgnorePaths(getPathsToIgnore(beer).toArray(new String[0]))
                .withMatcher("name", name -> name.contains());

        Example<Beer> example = Example.of(beer, exampleMatcher);

        List<Beer> beers = findAll(example);
        beers.forEach(b -> {
            b.getBrewer().getBeers().size();
            b.getCategory().getBeers().size();
        });
        return beers;
    }

    default List<String> getPathsToIgnore(Beer beer) {
        List<String> pathsToIgnore = new ArrayList<>();
        if (beer.getStock() == 0) {
            pathsToIgnore.add("stock");
        }
        if (beer.getPrice() == 0) {
            pathsToIgnore.add("price");
        }
        if (beer.getAlcohol() == 0) {
            pathsToIgnore.add("alcohol");
        }
        return pathsToIgnore;
    }

}
