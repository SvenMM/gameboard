package be.learningfever.explanation.beer.services;

import be.learningfever.explanation.beer.entities.BeerOrder;

import java.util.Map;

/**
 * @author Sven Wittoek
 * created on Tuesday, 04/05/2021
 */
public interface BeerOrderService {
    public void orderBeers(String orderName, long beerId, int numberOfBeers);
    public long orderBeers(String orderName, Map<Long, Integer> beersToOrder);
    BeerOrder findBeerOrder(long id);
}
