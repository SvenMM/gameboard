package be.learningfever.explanation.beer.services;

import be.learningfever.explanation.beer.entities.Beer;
import be.learningfever.explanation.beer.entities.BeerList;

import javax.validation.Valid;

/**
 * @author Sven Wittoek
 * created on Tuesday, 04/05/2021
 */
public interface BeerService {
    BeerList findAllBeers();
    BeerList findAllBeersByAlcohol(float alcohol);
    BeerList findAllBeersWithNameContaining(String name);
    Beer findBeer(long id);
    void saveBeer(@Valid Beer beer);
}
