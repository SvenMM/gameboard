package be.learningfever.explanation.beer.services;

import be.learningfever.explanation.beer.entities.Beer;
import be.learningfever.explanation.beer.entities.BeerList;
import be.learningfever.explanation.beer.exceptions.BeerNotFoundException;
import be.learningfever.explanation.beer.repositories.BeerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author Sven Wittoek
 * created on Tuesday, 04/05/2021
 */
@Service
public class BeerServiceImpl implements BeerService{
    private BeerRepository beerRepository;

    public BeerRepository getBeerRepository() {
        return beerRepository;
    }

    @Autowired
    public void setBeerRepository(BeerRepository beerRepository) {
        this.beerRepository = beerRepository;
    }

    @Override
    public BeerList findAllBeers() {
        return transformListToBeerList(beerRepository.findAll());
    }

    @Override
    public BeerList findAllBeersByAlcohol(float alcohol) {
        return transformListToBeerList(beerRepository.findAllBeersByAlcohol(alcohol));
    }

    @Override
    public BeerList findAllBeersWithNameContaining(String name) {
        return transformListToBeerList(beerRepository.findAllByNameContaining(name));
    }

    @Override
    public Beer findBeer(long id) {
        Optional<Beer> beer = beerRepository.findById(id);
        if (beer.isEmpty()) {
            throw new BeerNotFoundException();
        }
        return beer.get();
    }

    @Override
    public void saveBeer(Beer beer) {
        beerRepository.save(beer);
    }

    private BeerList transformListToBeerList(List<Beer> beers) {
        return new BeerList(beers);
    }

}
