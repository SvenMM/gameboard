package be.learningfever.explanation.beer.services;

import be.learningfever.explanation.beer.entities.Beer;
import be.learningfever.explanation.beer.entities.BeerOrder;
import be.learningfever.explanation.beer.entities.BeerOrderItem;
import be.learningfever.explanation.beer.exceptions.BeerNotFoundException;
import be.learningfever.explanation.beer.exceptions.InvalidOrderException;
import be.learningfever.explanation.beer.repositories.BeerOrderRepository;
import be.learningfever.explanation.beer.repositories.BeerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author Sven Wittoek
 * created on Tuesday, 04/05/2021
 */
@Service
public class BeerOrderServiceImpl implements BeerOrderService{
    private BeerOrderRepository beerOrderRepository;
    private BeerRepository beerRepository;

    public BeerOrderServiceImpl() {
    }

    public BeerOrderServiceImpl(BeerOrderRepository beerOrderRepository, BeerRepository beerRepository) {
        this.beerOrderRepository = beerOrderRepository;
        this.beerRepository = beerRepository;
    }

    public BeerOrderRepository getBeerOrderRepository() {
        return beerOrderRepository;
    }

    @Autowired
    public void setBeerOrderRepository(BeerOrderRepository beerOrderRepository) {
        this.beerOrderRepository = beerOrderRepository;
    }

    public BeerRepository getBeerRepository() {
        return beerRepository;
    }

    @Autowired
    public void setBeerRepository(BeerRepository beerRepository) {
        this.beerRepository = beerRepository;
    }

    @Override
    public void orderBeers(String orderName, long beerId, int numberOfBeers) {
        BeerOrderItem orderItem = createBeerOrderItem(beerId, numberOfBeers);
        BeerOrder beerOrder = createBeerOrder(orderName, List.of(orderItem));

        beerOrderRepository.save(beerOrder);
    }

    @Override
    public long orderBeers(String orderName, Map<Long, Integer> beersToOrder) {
        List<BeerOrderItem> orderItems = new ArrayList<>();
        for (Long key: beersToOrder.keySet()) {
            orderItems.add(createBeerOrderItem(key, beersToOrder.get(key)));
        }
        BeerOrder beerOrder = createBeerOrder(orderName, orderItems);
        beerOrderRepository.save(beerOrder);
        return beerOrder.getId();
    }

    @Override
    public BeerOrder findBeerOrder(long id) {
        Optional<BeerOrder> beerOrder = beerOrderRepository.findById(id);
        if (beerOrder.isEmpty()) {
            throw new InvalidOrderException("Order could not be found");
        }
        return beerOrder.get();
    }


    protected BeerOrderItem createBeerOrderItem(long beerId, int numberOfBeers) {
        Beer beer = beerRepository.findById(beerId).orElse(null);
        checkIfOrderIsPossible(numberOfBeers, beer);
        beer.setStock(beer.getStock() - numberOfBeers);
        return new BeerOrderItem(beer, numberOfBeers);
    }

    protected BeerOrder createBeerOrder(String name, List<BeerOrderItem> beerOrderItems) {
        return new BeerOrder(name, beerOrderItems);
    }

    protected void checkIfOrderIsPossible(int numberOfBeers, Beer beer) {
        if (Objects.isNull(beer)) {
            throw new BeerNotFoundException("Beer could not be found");
        }
        if (numberOfBeers <= 0 || beer.getStock() < numberOfBeers) {
            throw new InvalidOrderException("Ordered beers exceeds stock or is lower than or equal to 0");
        }
    }
}
