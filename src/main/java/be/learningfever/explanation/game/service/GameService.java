package be.learningfever.explanation.game.service;

import be.learningfever.explanation.game.Game;
import be.learningfever.explanation.game.GameType;

import java.util.Collection;

public interface GameService {
    Collection<Game> findAllGames();
    Collection<Game> findAllGamesByGameType(GameType gameType);
    Collection<Game> findAllGamesLimitedTo(long limit, long startPlace);
    Collection<Game> findAllGames(long limit, long startPlace, GameType gameType);
    Game findGame(long id);
    void saveGame(Game game);
    void deleteGame(long id);
}
