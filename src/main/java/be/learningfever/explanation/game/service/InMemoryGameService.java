package be.learningfever.explanation.game.service;

import be.learningfever.explanation.game.Game;
import be.learningfever.explanation.game.GameType;
import be.learningfever.explanation.game.repo.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class InMemoryGameService implements GameService{
    private GameRepository gameRepository;

    public GameRepository getGameRepository() {
        return gameRepository;
    }

    @Autowired
    public void setGameRepository(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    @Override
    public Collection<Game> findAllGames() {
        return gameRepository.getAllGames();
    }

    @Override
    public Collection<Game> findAllGamesByGameType(GameType gameType) {
        Collection<Game> allGames = findAllGames();
        Stream<Game> games = allGames.stream().filter(game -> game.getType().equals(gameType));
        return games.collect(Collectors.toList());
    }

    @Override
    public Collection<Game> findAllGamesLimitedTo(long limit, long startPlace) {
        Collection<Game> allGames = findAllGames();
        Stream<Game> games = allGames.stream().skip(startPlace).limit(limit);
        return games.collect(Collectors.toList());
    }

    @Override
    public Collection<Game> findAllGames(long limit, long amountToSkip, GameType gameType) {
        Collection<Game> allGames = checkIfGameTypeIsFilledInAndReturnAllGamesAccordingToType(gameType);
        Stream<Game> games = allGames.stream().skip(amountToSkip).limit(limit>0? limit : allGames.size());
        return games.collect(Collectors.toList());
    }

    @Override
    public Game findGame(long id) {
        return gameRepository.findGame(id);
    }

    @Override
    public void saveGame(Game game) {
        gameRepository.save(game);
    }

    @Override
    public void deleteGame(long id) {
        gameRepository.deleteGame(id);
    }


    private Collection<Game> checkIfGameTypeIsFilledInAndReturnAllGamesAccordingToType(GameType gameType) {
        if (Objects.nonNull(gameType)) {
            return findAllGamesByGameType(gameType);
        }
        return gameRepository.getAllGames();
    }
}
