package be.learningfever.explanation.game.repo;

import be.learningfever.explanation.game.Game;

import java.util.Collection;

public interface GameRepository {
    Game findGame(long id);
    Collection<Game> getAllGames();
    void save(Game game);
    void updateGame(Game game);
    void deleteGame(long id);
}
