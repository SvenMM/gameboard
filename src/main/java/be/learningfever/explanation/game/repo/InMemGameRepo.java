package be.learningfever.explanation.game.repo;

import be.learningfever.explanation.game.Game;
import be.learningfever.explanation.game.GameType;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.SortedMap;
import java.util.TreeMap;

@Repository
public class InMemGameRepo implements GameRepository{
    SortedMap<Long, Game> games = new TreeMap<>();

    public InMemGameRepo() {
        Game game = new Game("UNO", GameType.BOARD_GAME);
        save(game);
        game = new Game("Monopoly", GameType.BOARD_GAME);
        save(game);
        game = new Game("Dragon quest", GameType.VIDEO_GAME);
        save(game);
    }

    @Override
    public synchronized Game findGame(long id) {
        return games.get(id);
    }

    @Override
    public synchronized Collection<Game> getAllGames() {
        return games.values();
    }

    @Override
    public synchronized void save(Game game) {
        if (game.getId() != 0) {
            updateGame(game);
        } else {
            game.setId(createKey());
            games.put(game.getId(), game);
        }
    }

    @Override
    public synchronized void updateGame(Game game) {
        games.put(game.getId(), game);
    }

    @Override
    public synchronized void deleteGame(long id) {
        games.remove(id);
    }

    private long createKey(){
        try {
            return games.lastKey() + 1;
        } catch (NoSuchElementException nsee) {
//            lastKey() method throws NoSuchElementException when the SortedMap is empty, no keys are registered
            return 1;
        }
    }
}
