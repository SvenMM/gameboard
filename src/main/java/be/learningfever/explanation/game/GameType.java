package be.learningfever.explanation.game;

public enum GameType {
    BOARD_GAME,
    VIDEO_GAME;
}
