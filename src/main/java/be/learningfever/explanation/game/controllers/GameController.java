package be.learningfever.explanation.game.controllers;

import be.learningfever.explanation.game.Game;
import be.learningfever.explanation.game.GameType;
import be.learningfever.explanation.game.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;

@RestController
@RequestMapping("/Games")
public class GameController {
    private GameService gameService;

    public GameService getGameService() {
        return gameService;
    }

    @Autowired
    public void setGameService(GameService gameService) {
        this.gameService = gameService;
    }

    @GetMapping(path = "/{id}",
        produces = {MediaType.APPLICATION_JSON_UTF8_VALUE,
                    MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<Game> getGame(@PathVariable long id) {
        Game game = gameService.findGame(id);
        return new ResponseEntity<>(game, HttpStatus.OK);
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<Collection<Game>> getGames(@RequestParam(required = false, defaultValue = "0") long limit,
                                                     @RequestParam(required = false, defaultValue = "1") long page,
                                                     @RequestParam(required = false) GameType gameType) {
        Collection<Game> games = gameService.findAllGames(limit, limit * (page - 1), gameType);
        return new ResponseEntity<>(games, HttpStatus.OK);
    }

    @PutMapping(path = "{id:^\\d+$}",
            consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity updateGame(@PathVariable long id, @RequestBody Game game, HttpServletRequest request) {
//        Game id mag niet 0 zijn, omdat mijn repository een id van 0 registreert als een niet bestaande game en een nieuwe id gaat proberen toekennen
        if (checkIfGameIsValid(id, game)) {
            return ResponseEntity.badRequest().build();
        }
        gameService.saveGame(game);
        return ResponseEntity.ok().build();
    }

    @PatchMapping(path = "{id:^\\d+$}",
            consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity updateGamePartially(@PathVariable long id, @RequestBody Game game, HttpServletRequest request) {
//        Game id mag niet 0 zijn, omdat mijn repository een id van 0 registreert als een niet bestaande game en een nieuwe id gaat proberen toekennen
        if (checkIfGameIsValid(id, game)) {
            return ResponseEntity.badRequest().build();
        }
        Game original = gameService.findGame(id);
        if (game.getType() != null) {
            original.setType(game.getType());
        }
        if (game.getTitle() != null) {
            original.setTitle(game.getTitle());
        }
        gameService.saveGame(original);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(path = "{id}")
    public ResponseEntity deleteGame(@PathVariable long id) {
        gameService.deleteGame(id);
        return ResponseEntity.ok().build();
    }

    private boolean checkIfGameIsValid(@PathVariable long id, @RequestBody Game game) {
        return game == null || game.getId() <= 0 || game.getId() != id;
    }


}
